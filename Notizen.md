# Fehlende Bestandteile
- Keypad und LCD-Display: 
  - https://www.circuitbasics.com/how-to-set-up-a-keypad-on-an-arduino/
  - Missing Pins? https://www.youtube.com/watch?v=n9Bq1kHYsJk
- Fernbedienung: https://www.circuitbasics.com/arduino-ir-remote-receiver-tutorial/

- Zeit einstellen
- Code eingeben statt nur draufhauen
  - pro Team ein bestimmter Code
- Pause Modus *
- Recht und Unten Tasten verstecken, nur zum Spiel einstellen

# Box
- 25x25 cm
- Klappe über Tastenfeld
- Folie über Display und Tasten, dass auch bei leichtem Regen spielbar
- durchsichtige Kuppel über Lampe