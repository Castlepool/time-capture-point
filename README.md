# Time Capture Point

## Beispiel-Verwendung
1. Spielleiter: verbindet Arduino mit Stromquelle (Batterie/Powerbank/...)
2. LED-Lampe: blinkt weiß
3. LCD-Display: "Bereit"
4. Mitarbeiter tippt auf Eingabefeld: 
   1. "A", dann "7", dann mit "#" bestätigen => ein Team braucht 7 Minuten auf seinem Timer um zu siegen (Standard: 7)
   2. "B", dann "123", dann nochmal "B", dann "456", mit "#" bestätigen => Team Rot hat Code 123, Team Blau hat 456. Hinweise:
      1. Durch wiederholtes Drücken von "B" wählt man das Team aus, für das man einen Code eingeben möchte
      2. Ein Teamcode MUSS aus 3 Zahlen bestehen
   3. "C" => Summer an/aus (Standard: an)
   4. "D" => Neues Spiel (Timer werden zurückgesetzt und angehalten, aktuelle Einstellungen bleiben erhalten)
5. Team rot gibt Code "123" ein, Lampe beginnt rot zu leuchten, roter Timer startet (zählt von oben runter)
6. Team blau gibt Code "456" ein, Lampe leuchtet blau, roter Timer pausiert, blauer Timer startet
7. Blauer Timer erreicht 7 Minuten => Team blau hat gewonnen
8. LED-Lampe: blinkt blau und der Summer piept dauerhaft (bis man ein neues Spiel startet)
9.  LCD-Display: Erste Zeile "Team Blau gewinnt!", zweite Zeile zeigt verbliebene Zeit des zweitbesten Teams
10. Der Spielleiter drückt "C", um den Summer zu stoppen
11. Das Gewinnerteam wird weiterhin angezeigt
12. Der Spielleiter drückt "D", um ein neues Spiel zu starten, Timer werden dann zurückgesetzt und angehalten und man kann mit den selben Voreinstellungen ein neues Spiel spielen

Generelle Hinweise:
- Maximal 5 Teams (bzw. 5 Farben)
- Jedes Team hat eine Farbe, in der die Lampe leuchtet, während ihr Timer aktiv ist
- Das derzeit zweitbeste Team wird auch mit seinem Timer-Stand auf dem Display angezeigt
- Nur Teams, für die explizit ein Code eingestellt wurde (mit "B"), können ihren Timer aktivieren
- Ein Neustart des Geräts (Stromzufuhr entfernen) setzt alle Einstellungen zurück, inklusive der eingestellten Team-Codes!
