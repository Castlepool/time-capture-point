#include <Keypad.h>
#include <LiquidCrystal.h>

/*
 * Time Capture Point Arduino
 *
 * Created: 2020/12/06
 * Author: Samuel Mergenthaler
 * 
 * For Arduino setup, see https://www.tinkercad.com/things/lBYPC5LNFCB-arduinoeinnahmepunkt/editel
 */

// the following variables can be configured by the user
int secondsToWin = 600;
byte buzzerEnabled = 1;
byte pause = 0;

// Setup Keypad
const byte INPUT_LENGTH = 4;
char input[INPUT_LENGTH];

const byte ROWS = 4;
const byte COLS = 4;

char hexaKeys[ROWS][COLS] = {
    {'1', '2', '3', 'A'},
    {'4', '5', '6', 'B'},
    {'7', '8', '9', 'C'},
    {'*', '0', '#', 'D'}
};

byte rowPins[ROWS] = {2, 3, 4, 5};
byte colPins[COLS] = {6, 7, 8, 9};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

// setup display
const int rs = 14, en = 15, d4 = 16, d5 = 17, d6 = 18, d7 = 19;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// Teams definition
struct RGBColor {
    const int red;
    const int green;
    const int blue;
};

struct Team {
    const char name[6];
    long seconds;
    char code[4];
    const RGBColor color;
    const char *nameWithFiveChars;
};

const int NUMBER_OF_TEAMS = 5;

struct Team teams[NUMBER_OF_TEAMS] = {
    { "Rot", secondsToWin, "", {255, 0, 0}, "Rot  " },
    { "Blau", secondsToWin, "", {0, 0, 255}, "Blau " },
    { "Gruen", secondsToWin, "", {0, 255, 0}, "Gruen" },
    { "Gelb", secondsToWin, "", {255, 255, 0}, "Gelb " },
    { "Cyan", secondsToWin, "", {0, 255, 255}, "Cyan " },
};

// helper variable for periodic tasks
unsigned long previousMillis = 0;

// index of currently active team
volatile int activeTeamIndex = -1;

// index of best team appart from active one
volatile int bestNonActiveTeamIndex = -1;

// index of last winner team
volatile int winnerTeamIndex = -1;

// just to make blinking light possible
volatile int blinkState = 0;

// pins of the single rgb led
const int redPin = 10;
const int greenPin = 11;
const int bluePin = 12;

// pin for buzzer
const int buzzerPin = 13;

void setRGBColor(int red, int green, int blue) {
    digitalWrite(redPin, red);
    digitalWrite(greenPin, green);
    digitalWrite(bluePin, blue);
}

void setTeamColor(int teamIndex) {
    if(teamIndex != -1) {
        // set rgb led to color of given team
        setRGBColor(teams[teamIndex].color.red, teams[teamIndex].color.green, teams[teamIndex].color.blue);
    } else {
        // set rgb led to white
        setRGBColor(255, 255, 255);
    }
}

void clearKeypadInput() {
    memset(input, 0, INPUT_LENGTH);
}

void clearLCDLine(byte line) {
    lcd.setCursor (0, line);
    for (int i = 0; i < 16; ++i) {
        lcd.write(' ');
    }
}

void printMainMenu() {
    lcd.clear();
    lcd.print("Bereit!");
}

void printActiveTeamCounter() {
    clearLCDLine(0);
    lcd.setCursor(0, 0);
    lcd.print(teams[activeTeamIndex].nameWithFiveChars);
    lcd.print(" ");
    lcd.print(teams[activeTeamIndex].seconds / 60);
    lcd.print("m");
    lcd.print(teams[activeTeamIndex].seconds % 60);
    lcd.print("s");
    if(strlen(input) == 0 && bestNonActiveTeamIndex != -1) {
        clearLCDLine(1);
        lcd.setCursor(0, 1);
        lcd.print(teams[bestNonActiveTeamIndex].nameWithFiveChars);
        lcd.print(" ");
        lcd.print(teams[bestNonActiveTeamIndex].seconds / 60);
        lcd.print("m");
        lcd.print(teams[bestNonActiveTeamIndex].seconds % 60);
        lcd.print("s");
    }
}

void printGameResult(const char *winnerName) {
    clearLCDLine(0);
    lcd.setCursor(0, 0);
    lcd.print(winnerName);
    lcd.print(" gewinnt!");
    if(bestNonActiveTeamIndex != -1) {
        clearLCDLine(1);
        lcd.setCursor(0, 1);
        lcd.print(teams[bestNonActiveTeamIndex].nameWithFiveChars);
        lcd.print(" ");
        lcd.print(teams[bestNonActiveTeamIndex].seconds / 60);
        lcd.print("m");
        lcd.print(teams[bestNonActiveTeamIndex].seconds % 60);
        lcd.print("s");
    }
}

////
//  Configuration functions 
////

void configureSecondsToWin() {
    lcd.clear();
    lcd.print("Minuten:");
    lcd.setCursor(13, 0);
    lcd.print(secondsToWin / 60);
    lcd.setCursor(0, 1);

    int newMinutesToWin;

    // remember numbers entered till confirmed with #
    while(true) {
        char customKey = customKeypad.getKey();
        if(customKey) {
            if(customKey == '#') {
                if(strlen(input) == 0) {
                    clearKeypadInput();
                    printMainMenu();
                    return;
                }
                newMinutesToWin = atoi(input);
                if(newMinutesToWin < 1 || newMinutesToWin > 545) {
                    clearKeypadInput();
                    clearLCDLine(1);
                    lcd.setCursor(0, 1);
                    lcd.print("Falsche Eingabe!");
                    delay(2000);
                    clearLCDLine(1);
                    lcd.setCursor(0, 1);
                    continue;
                }
                break;
            } else if(customKey == 'A') {
                lcd.clear();
                lcd.print("Kacke!");
                delay(2000);
                printMainMenu();
                return;
            } else if(customKey == '*') {
                clearKeypadInput();
                clearLCDLine(1);
                lcd.setCursor(0, 1);
            } else if(customKey != 'B' && customKey != 'C' && customKey != 'D') {
                if(strlen(input) == 0) {
                    clearLCDLine(1);
                    lcd.setCursor(0, 1);
                    lcd.print("Eingabe: ");
                }
                input[strlen(input)] = customKey;
                lcd.print(customKey);
            }
        }
    }

    int newSecondsToWin = newMinutesToWin * 60;

    for (size_t i = 0; i < NUMBER_OF_TEAMS; i++) {
        int currentElapsedSeconds = secondsToWin - teams[i].seconds;
        if(currentElapsedSeconds <= 0) {
            currentElapsedSeconds = 0;
        }
        teams[i].seconds = newSecondsToWin - currentElapsedSeconds;
    }

    secondsToWin = newSecondsToWin;

    //Serial.println("Game length entered (in minutes): ");
    //Serial.println(secondsToWin / 60);

    clearKeypadInput();
    printMainMenu();
}

unsigned int toggleBuzzer() {
    buzzerEnabled = !buzzerEnabled;

    lcd.clear();
    if(buzzerEnabled) {
        lcd.print("Summer ein");
    } else {
        lcd.print("Summer aus");
    }

    delay(1000);

    printMainMenu();
}

void changeTeamCode() {
    lcd.clear();
    if(activeTeamIndex == -1) {
        activeTeamIndex = 0;
    }
    lcd.print("Team: ");
    lcd.print(teams[activeTeamIndex].name);
    lcd.setCursor(13, 0);
    lcd.print(teams[activeTeamIndex].code);
    lcd.setCursor(0, 1);

    // remember numbers entered till confirmed with #
    while(true) {
        char customKey = customKeypad.getKey();
        if(customKey) {
            if(customKey == '#') {
                activeTeamIndex = -1;
                clearKeypadInput();
                printMainMenu();
                // print new team codes
                //for (size_t i = 0; i < NUMBER_OF_TEAMS; i++) {
                //    Serial.println("");
                //    Serial.print(teams[i].name);
                //    Serial.print(": ");
                //    Serial.print(teams[i].code);
                //}
                //Serial.println("");
                //Serial.println("");
                return;
            }
            if(customKey == 'B') {
                activeTeamIndex = (activeTeamIndex + 1) % NUMBER_OF_TEAMS;
                lcd.clear();
                lcd.print("Team: ");
                lcd.print(teams[activeTeamIndex].name);
                lcd.setCursor(13, 0);
                lcd.print(teams[activeTeamIndex].code);
                lcd.setCursor(0, 1);
                continue;
            } else if(customKey == '*') {
                clearKeypadInput();
                clearLCDLine(1);
                lcd.setCursor(0, 1);
            } else if(customKey != 'A' && customKey != 'C' && customKey != 'D') {
                if(strlen(input) == 0) {
                    lcd.print("Eingabe: ");
                }
                input[strlen(input)] = customKey;
                lcd.print(customKey);
                if(strlen(input) > 2) {
                    strcpy(teams[activeTeamIndex].code, input);

                    delay(1000);
                    clearKeypadInput();
                    lcd.clear();
                    lcd.print("Team: ");
                    lcd.print(teams[activeTeamIndex].name);
                    lcd.setCursor(13, 0);
                    lcd.print(teams[activeTeamIndex].code);
                    lcd.setCursor(0, 1);
                }
            }
        }
    }
}

void resetGame() {
    activeTeamIndex = -1;
    winnerTeamIndex = -1;
    bestNonActiveTeamIndex = -1;

    // reset all timers
    for (size_t i = 0; i < NUMBER_OF_TEAMS; i++) {
        teams[i].seconds = secondsToWin;
    }

    lcd.clear();
    lcd.print("Neustart...");
    delay(1000);
    clearKeypadInput();
    printMainMenu();
}

// prepare pins, interrupts etc.
void setup()
{
    // open serial port at 9600 bps to print some debug information
    //Serial.begin(9600); // open the serial port at 9600 bps:

    //pinMode(buttonPin, INPUT);
    pinMode(redPin, OUTPUT);
    pinMode(greenPin, OUTPUT);
    pinMode(bluePin, OUTPUT);

    pinMode(buzzerPin, OUTPUT);

    setRGBColor(255, 255, 255);

    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    printMainMenu();
}

void loop()
{
    while (true) {
        // check for new input character
        char customKey = customKeypad.getKey();

        // update state according to new input character
        if (customKey){
            // configure game length in minutes
            if(customKey == 'A') {
                configureSecondsToWin();
            } else if(customKey == 'B') {
                changeTeamCode();
            } else if(customKey == 'C') {
                toggleBuzzer();
            } else if(customKey == 'D') {
                resetGame();
            } else if(customKey == '*') {
                if(strlen(input) > 0) {
                    clearKeypadInput();
                    clearLCDLine(1);
                }
            } else if(customKey != '#') {
                // add new character to previous input characters
                input[strlen(input)] = customKey;

                clearLCDLine(1);
                lcd.setCursor(0, 1);
                lcd.print("Eingabe: ");
                lcd.print(input);

                 // update active team, using the code entered
                if(strlen(input) > 2) {
                    // check whether a team code was entered
                    for (size_t i = 0; i < NUMBER_OF_TEAMS; i++) {
                        // if team code found in input
                        if(strcmp(input, teams[i].code) == 0) {
                            //Serial.println(input);
                            //Serial.println(teams[i].name);

                            // increment active team index (if stepped over last team index, go back to first team index)
                            activeTeamIndex = i;

                            // set rgb led to color of active team
                            setTeamColor(activeTeamIndex);

                            // figure out best team appart from active one
                            int bestNonActiveTime = secondsToWin;
                            for (size_t j = 0; j < NUMBER_OF_TEAMS; j++) {
                                // find best team except from active one
                                if(j != activeTeamIndex && teams[j].seconds < bestNonActiveTime) {
                                    bestNonActiveTime = teams[j].seconds;
                                    bestNonActiveTeamIndex = j;
                                }
                            }

                            printActiveTeamCounter();

                            break;
                        }
                    }
                    clearKeypadInput();
                    clearLCDLine(1);
                }
            }
        }

        // all other steps are only done every other second
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis >= 1000) {
            previousMillis = currentMillis;

            // if no team has pressed the button yet
            if (activeTeamIndex == -1) {
                //Serial.println("Waiting for team to press button...");

                // set rgb led to color of active team
                if(blinkState == 0) {
                    setTeamColor(winnerTeamIndex);
                    blinkState = 1;
                }
                else {
                    setRGBColor(0, 0, 0);
                    blinkState = 0;
                }
            } else {
                // decrement timer of active team
                teams[activeTeamIndex].seconds--;
                printActiveTeamCounter();

                // check wether active team reached required seconds to win
                if (teams[activeTeamIndex].seconds <= 0) {
                    winnerTeamIndex = activeTeamIndex;

                    printGameResult(teams[activeTeamIndex].name);
                    clearKeypadInput();

                    if(buzzerEnabled == 1) {
                        // buzzer sound for a few seconds
                        tone(buzzerPin, 1000); // Send 1KHz sound signal...
                    }

                    byte readyToRestart = 0;
                    while(true) {
                        // blink in winner team's color
                        unsigned long currentMillis = millis();
                        if (currentMillis - previousMillis >= 500) {
                            previousMillis = currentMillis;
                                // set rgb led to color of active team
                                if(blinkState == 0) {
                                    setTeamColor(winnerTeamIndex);
                                    blinkState = 1;
                                }
                                else {
                                    setRGBColor(0, 0, 0);
                                    blinkState = 0;
                                }
                        }

                        // controls for game master to to continue
                        char customKey = customKeypad.getKey();
                        if(customKey) {
                            if(customKey == 'C') { // ... until button 'C' pressed
                                noTone(buzzerPin); // Stop sound
                                readyToRestart = 1;
                            } else if(readyToRestart && customKey == 'D') {
                                noTone(buzzerPin); // Stop sound
                                resetGame();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
